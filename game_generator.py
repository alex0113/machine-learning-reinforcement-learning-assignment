from random import random, randint, choice
from argparse import ArgumentParser
from game_utils import valid_coordinates, ACTIONS, ACTION_EFFECTS, EXIT_CODE


def inaccessible_legal_moves(i, j, height, width):
    legal_actions = []
    for action in ACTIONS:
        if action == "STAY":
            continue

        next_i = i + ACTION_EFFECTS[action][0]
        next_j = j + ACTION_EFFECTS[action][1]

        if not valid_coordinates(next_i, next_j, height, width):
            continue
        legal_actions.append(action)
    return legal_actions


def generate_inaccessible(matrix, i, j, height, width,  blocks_left):
    if blocks_left == 0:
        return
    if not matrix[i][j] == " ":
        return
    matrix[i][j] = "*"
    legal_moves = inaccessible_legal_moves(i, j, height, width)
    if len(legal_moves) == 0:
        return
    next_action = choice(legal_moves)

    next_i = i+ ACTION_EFFECTS[next_action][0]
    next_j = j+ ACTION_EFFECTS[next_action][1]
    generate_inaccessible(matrix, next_i, next_j, height, width, blocks_left-1)

def write_map(matrix, height, width, guard_position, filename, permissions_arg):
    with open(filename, permissions_arg) as f:
        first_line = str(height) + " " + str(width) + "\n"
        f.write(first_line)
        second_line = str(guard_position[0]) + " " + str(guard_position[1]) + "\n"
        f.write(second_line)
        for i in range(height):
            for j in range(width):
                f.write(matrix[i][j])
            f.write("\n")
        f.close()
def add_guardian_to_map (matrix, height, width):
    while True:
        rand_i = randint(0, height-1)
        rand_j = randint(0, width-1)
        if matrix[rand_i][rand_j] == " ":
            return (rand_i, rand_j)

def add_player_to_map (matrix, height, width, guardian_position):
    while True:
        rand_i = randint(0, height-1)
        rand_j = randint(0, width-1)
        if matrix[rand_i][rand_j] == " " and not (rand_i, rand_j) == guardian_position:
            return (rand_i, rand_j)

def add_portal_to_map (matrix, height, width, guardian_position):
    while True:
        rand_i = randint(0, height-1)
        rand_j = randint(0, width-1)
        if matrix[rand_i][rand_j] == " " and not (rand_i, rand_j) == guardian_position:
            return (rand_i, rand_j)

def generate_map (args):
    print(args.guardians, args.rooms)
    assert(args.guardians <= args.rooms)
    rooms = args.rooms
    rooms_with_guardians = []
    while(len(rooms_with_guardians) < args.guardians):
        room_choice = choice(range(0, args.rooms))
        if not room_choice in rooms_with_guardians:
            rooms_with_guardians.append(room_choice)

    player_room = randint(0, rooms-1)
    rooms_without_portals = list(range(0, args.rooms))
    rooms_without_portals.remove(player_room)
    portals = {}
    portals_left = args.portals
    last_room = player_room
    while len(rooms_without_portals) > 0:
        room_choice = choice(rooms_without_portals)
        rooms_without_portals.remove(room_choice)
        if not last_room == None:
            portals[(last_room, room_choice)] = None
            portals_left -= 1
        last_room = room_choice

    portals[(last_room, EXIT_CODE)] = None
    portals_left -= 1
    while portals_left > 0 and args.rooms > 1:
        all_rooms = list(range(0, args.rooms))
        room1 = choice(all_rooms)
        all_rooms.remove(room1)
        room2 = choice(all_rooms)
        portals[(room1, room2)] = None
        portals_left -= 1



    treasures_prob = args.treasure_prob
    inaccessible_spaces_prob = args.inaccessible_spaces_prob
    with open(args.map_file, "w+") as f:
        f.write(str(rooms) + "\n")

    player_position = None
    for room_id in range(rooms):
        minimum = args.room_minimum_size
        maximum = args.room_maximum_size
        width = randint(minimum, maximum)
        height = randint(minimum, maximum)
        room_matrix = [["0" for x in range(width)] for y in range(height)]
        for i in range(height):
            for j in range(width):
                element = ""
                if i == 0 or j == 0 or i == height-1 or j == width-1:
                    element = "*"
                elif random() < treasures_prob:
                    element = "$"
                else:
                    element = " "
                room_matrix[i][j] = element


        for i in range(height):
            for j in range(width):
                if random() < inaccessible_spaces_prob:
                    generate_inaccessible(room_matrix, i, j, height, width, args.inaccessible_space_max_size)
        guardian_position = (-1, -1)
        if room_id in rooms_with_guardians:
            guardian_position = add_guardian_to_map(room_matrix, height, width)
        if room_id == player_room:
            player_position = add_player_to_map(room_matrix, height, width, guardian_position)

        for portal in portals:
            p1, p2 = portal
            if p1 == room_id:
                portal_position = add_portal_to_map(room_matrix, height, width, guardian_position)
                if portals[portal] == None:
                    portals[portal] = (portal_position, None)
                else:
                    portals[portal] = (portal_position, portals[portal][1])
            if p2 == room_id:
                portal_position = add_portal_to_map(room_matrix, height, width, guardian_position)
                if portals[portal] == None:
                    portals[portal] = (None, portal_position)
                else:
                    portals[portal] = (portals[portal][0], portal_position)
            if p1 == EXIT_CODE:
                if portals[portal] == None:
                    portals[portal] = ((-1, -1), None)
                elif portals[portal][0] == None:
                    portals[portal] = ((-1, -1), portals[portal][1])

            if p2 == EXIT_CODE:
                if portals[portal] == None:
                    portals[portal] = ((-1, -1), None)

                elif portals[portal][1] == None:
                    portals[portal] = (portals[portal][0], (-1, -1))

        write_map(room_matrix, height, width, guardian_position, args.map_file, "a+")
    print(portals.values())
    print(player_room, player_position)
    assert(not player_position == None)
    player_line = str(player_room) + " " + str(player_position[0]) + " " + str(player_position[1]) + "\n"
    with open(args.map_file, "a+") as f:
        f.write(player_line)

        # Print portals !!!
        f.write(str(len(portals)) + "\n")
        for portal in portals:
            portal_line = str(portal[0]) + " " + str(portal[1]) + "\n"
            f.write(portal_line)
            portal_second_line = str(portals[portal][0][0]) + " " + str(portals[portal][0][1]) + "\n"
            f.write(portal_second_line)
            portal_third_line = str(portals[portal][1][0]) + " " + str(portals[portal][1][1]) + "\n"
            f.write(portal_third_line)


    f.close()





if __name__ == "__main__":
    parser = ArgumentParser()
    # Training and evaluation episodes
    parser.add_argument("--map_file", type = str, default = "out_map.txt",
                        help = "File to write map to.")
    parser.add_argument("--rooms", type = int, default = 1,
                        help = "Number of rooms")
    parser.add_argument("--guardians", type = int, default = 1,
                        help = "Number of guardians")
    parser.add_argument("--room_minimum_size", type = int, default = 5,
                        help = "Minimum size of a side")
    parser.add_argument("--room_maximum_size", type = int, default = 30,
                        help = "Maximum size of a side")
    parser.add_argument("--treasure_prob", type = float, default = 0.08,
                        help = "Number of treasures")
    parser.add_argument("--portals", type = int, default = 8,
                        help = "Number of portals")
    parser.add_argument("--inaccessible_spaces_prob", type = float, default = 0.05,
                        help = "Percentage of inaccessible_spaces")
    parser.add_argument("--inaccessible_space_max_size", type= int, default = 5,
                        help = "Maximum size of inaccessible space")

    args = parser.parse_args()
    generate_map(args)

from argparse import ArgumentParser
from game import Game
from game_utils import WIN_REWARD, LOOSE_REWARD, MAX_STRATEGY, RANDOM_STRATEGY, EPSILON_STRATEGY, MAX_STRATEGY
from time import sleep

def q_learning(args):
    train_scores = []
    eval_scores = []
    learning_rates = []
    Q = {}
    learning_rate = 0
    for l in range(10):
        lr_score = 0
        lr_average_score = 0
        learning_rates.append(learning_rate)
                                                          # for each episode ...
        for train_ep in range(1, args.train_episodes + 1):

                                                    # ... get the initial state,
            game = Game(args, Q)
            score = 0

                                           # while current state is not terminal
            while not game.is_over():
                reward, action, old_state, next_state = game.round(EPSILON_STRATEGY)
                if not (old_state, action) in Q:
                    Q[(old_state, action)] = 0

                score += reward

                # TODO (1) : Q-Learning
                max_q_prim = -9999
                next_actions = game.get_legal_actions()
                for next_action in next_actions:
                    if not (next_state, next_action) in Q:
                        r = 0
                    else:
                        r = Q[(next_state, next_action)]
                    if r > max_q_prim:
                        max_q_prim = r

                old_value = Q[(old_state, action)]
                if max_q_prim == -9999:
                    print("aoooleeeuuu")
                    return

                Q[(old_state,action)] = old_value + learning_rate * (reward + args.discount*max_q_prim-old_value)
                                               # display current state and sleep

                if args.verbose:
                    print(str(game)); sleep(args.sleep)
            print("Dimensiuneeee" + str(len(Q)))
            print("Episode %6d / %6d" % (train_ep, args.train_episodes))
            lr_score += score
        #print(Q.values())
                                                 # evaluate the greedy policy
            if train_ep % args.eval_every == 0:
                avg_score = .0

                # TODO (4) : Evaluate
                for i in range(int(args.eval_episodes)):
                    print(len(Q))
                    game_train = Game(args, Q)
                    score_train = 0

                    while not game_train.is_over():
                        reward, action, old_state, new_state = game_train.round(MAX_STRATEGY)
                        score_train += reward

                    avg_score += score_train
                #print(score)
                avg_score /= args.eval_episodes
                lr_average_score += avg_score
        lr_score/= int(args.train_episodes)
        train_scores.append(lr_score)

        lr_average_score/=int(args.train_episodes)
        eval_scores.append(lr_average_score)
        learning_rate += 0.1
        print(learning_rate)
        print(len(eval_scores))
        print(len(train_scores))

    for key in Q:
        (state, action) = key
        value = Q[key]
        with open("lala.txt", "a+") as f:
            f.write (str(value))
            f.write("\n" + action)
            f.write(state + "\n")
    # --------------------------------------------------------------------------
    if args.final_show:
        game_train = Game(args, Q)
        final_score = 0
        while not game_train.is_over():
           reward, action, old_state, new_state = game_train.round(MAX_STRATEGY)
           final_score += reward
           print(str(game_train)); sleep(args.sleep)
    if args.plot_scores:
        from matplotlib import pyplot as plt
        import numpy as np
        plt.xlabel("Learning rate")
        plt.ylabel("Average score")
        #plt.plot(
        #    np.linspace(0, 1,10),
        #    np.convolve(train_scores, [0.2,0.2,0.2,0.2,0.2], "same"),
        #    linewidth = 1.0, color = "blue"
        #)
        plt.plot(
            np.linspace(0, 1, 10, endpoint=False),
            train_scores,
            linewidth = 1.0, color = "blue"
        )
        plt.plot(
            np.linspace(0, 1, 10, endpoint=False),
            eval_scores,
            linewidth = 2.0, color = "red"
        )
        plt.show()



if __name__ == "__main__":
    parser = ArgumentParser()
    # Training and evaluation episodes
    parser.add_argument("--map_file", type = str, default = "maps/scenario2.txt",
                        help = "File to write map to.")
    parser.add_argument("--log_file", type = str, default = "out_log.txt",
                        help = "File to write log to.")
    parser.add_argument("--strategy", type = str, default = "RANDOM",
                        help = "Player's strategy: RANDOM, MAX or EPSILON")
    parser.add_argument("--max_rounds", type = int, default =3000,
                        help = "Number of rooms")

    parser.add_argument("--perception_ray", type = int, default = 5,
                        help = "Size of perception ray")
    parser.add_argument("--perception_ray_guardian", type = int, default = 100,
                        help = "Size of perception ray")
    # Meta-parameters
    parser.add_argument("--learning_rate", type = float, default = 0.1,
                        help = "Learning rate")
    parser.add_argument("--discount", type = float, default = 0.99,
                        help = "Value for the discount factor")
    parser.add_argument("--epsilon", type = float, default = 0.05,
                        help = "Probability to choose a random action.")
    # Training and evaluation episodes
    parser.add_argument("--train_episodes", type = int, default = 1000,
                        help = "Number of episodes")
    parser.add_argument("--eval_every", type = int, default = 10,
                        help = "Evaluate policy every ... games.")
    parser.add_argument("--eval_episodes", type = float, default = 10,
                        help = "Number of games to play for evaluation.")

    # Display
    parser.add_argument("--verbose", dest="verbose",
                        action = "store_true", help = "Print each state")
    parser.add_argument("--plot", dest="plot_scores", action="store_true",
                        help = "Plot scores in the end")
    parser.add_argument("--sleep", type = float, default = 0.1,
                        help = "Seconds to 'sleep' between moves.")
    parser.add_argument("--final_show", dest = "final_show",
                        action = "store_true",
                        help = "Demonstrate final strategy.")

    args = parser.parse_args()
    q_learning(args)
        

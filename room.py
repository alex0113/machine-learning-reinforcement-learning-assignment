from random import choice, random
from game_utils import GUARDIAN_CHAR, PLAYER_CHAR, PORTAL_CHAR, PLAYER_INCOGNITO_CHAR, ACTIONS, ACTION_EFFECTS,\
EXIT_CODE, valid_coordinates, distance_between,  MOVE_REWARD, TREASURE_CHAR, TREASURE_REWARD, PARADISE_ROOM,\
LOOSE_REWARD, WIN_REWARD, RANDOM_STRATEGY, MAX_STRATEGY, EPSILON_STRATEGY, distance_lt
from math import fabs

class Room ():
    def __init__(self, room_id, matrix, guard_position, perception_ray, perception_ray_guardian, epsilon):
        self.room_id = room_id
        self.matrix = matrix
        self.perception_ray = perception_ray
        self.perception_ray_guardian = perception_ray_guardian
        self.height = len(matrix)
        self.width = len(matrix[0])

        self.guard_position = guard_position
        self.player_position = (-1, -1)
        self.portals = {}
        self.epsilon = epsilon

    def find_portal(self, location):
        for portal in self.portals:
            if self.portals[portal][0] == location:
                return portal[1], self.portals[portal][1][0],  self.portals[portal][1][1]
        return None

    def __str__(self):
        ret_s = "Room " + str(self.room_id) + "\n"
        for i in range(self.height):
            for j in range(self.width):
                if i==self.guard_position[0] and j==self.guard_position[1]:
                    ret_s += GUARDIAN_CHAR
                elif not self.find_portal((i, j)) == None:
                    ret_s += PORTAL_CHAR
                elif i == self.player_position[0] and j== self.player_position[1]:
                    if distance_between(self.guard_position, self.player_position) <= self.perception_ray:
                        ret_s += PLAYER_CHAR
                    else:
                        ret_s += PLAYER_INCOGNITO_CHAR
                else:
                    ret_s += self.matrix[i][j]

            ret_s += "\n"
        return ret_s
    def compute_state(self):
        state = ""
        for i in range(self.height):
            for j in range(self.width):
                p_i, p_j = self.player_position
                if fabs(p_i - i) < self.perception_ray and fabs(p_j - j)< self.perception_ray:
                    if (i, j) == self.guard_position:
                        state += GUARDIAN_CHAR
                    elif (i, j) == self.player_position:
                        state += PLAYER_CHAR
                    elif self.matrix[i][j] == TREASURE_CHAR:
                        state += " "
                    else:
                        state += self.matrix[i][j]
            state += "\n"
        return state

    # Legal moves for both guardian and Gigel
    def legal_next_moves(self, position):
        legal_actions = []
        (i, j) = position
        for action in ACTIONS:
            # PAAANIIIC !!!
            if action == "STAY":
                continue
            next_i = i + ACTION_EFFECTS[action][0]
            next_j = j + ACTION_EFFECTS[action][1]

            if not valid_coordinates(next_i, next_j, self.height, self.width):
                continue
            if self.matrix[next_i][next_j] == "*":
                continue

            legal_actions.append(action)
        return legal_actions

    def guardian_optimised_moves (self, gigel_position):
        legal_moves = self.legal_next_moves(self.guard_position)
        gigel_i, gigel_j = gigel_position
        guard_i, guard_j = self.guard_position

        # Guardian is on the same axis with Gigel
        if gigel_i == guard_i and gigel_j < guard_j and "LEFT" in legal_moves:
            return ["LEFT"]
        elif gigel_i == guard_i and gigel_j > guard_j and "RIGHT" in legal_moves:
            return ["RIGHT"]
        elif gigel_j == guard_j and gigel_i > guard_i and "DOWN" in legal_moves:
            return ["DOWN"]
        elif gigel_j == guard_j and gigel_i < guard_i and "UP" in legal_moves:
            return ["UP"]

        actions = []
        if gigel_i < guard_i:
            actions.append("UP")
        elif gigel_i > guard_i:
            actions.append("DOWN")

        if gigel_j< guard_j:
            actions.append("LEFT")
        elif gigel_j>guard_j:
            actions.append("RIGHT")
        return actions





        #Updates position of the guardian
    def move_guardian(self, gigel_position = None):
        if self.guard_position == (-1, -1) or gigel_position ==  None:
            return True

        guard_i, guard_j = self.guard_position

        # Guardian goes towards Gigel
        if distance_lt(self.guard_position, gigel_position, self.perception_ray_guardian):
            actions = self.guardian_optimised_moves(gigel_position)
        else:
            actions = self.legal_next_moves(self.guard_position)

        if len(actions) == 0:
            return True
        action = choice(actions)

        new_guard_i = guard_i + ACTION_EFFECTS[action][0]
        new_guard_j = guard_j + ACTION_EFFECTS[action][1]

        self.guard_position =  (new_guard_i, new_guard_j)
        if self.guard_position == gigel_position:
            return False
        return True

    def best_action(self, Q, state, legal_actions):
        # TODO (3) : Best action
        max_val = -9999
        max_action = choice(legal_actions)
        for action in legal_actions:
            if (state, action) in Q:
                reward = Q[(state, action)]
            else:
                continue

            if reward > max_val:
                max_val = reward
                max_action = action

        if not (state, max_action) in Q:
            Q[(state, max_action)] = 0

        return max_action

    def epsilon_action(self, Q, state, legal_actions):
        # TODO (2) : Epsilon greedy


        not_explored_actions = []
        for action in legal_actions:
            if not (state, action) in Q:
                not_explored_actions.append(action)

        if len(not_explored_actions) > 0:
            random_action = choice(not_explored_actions)
            #print("unexplored randooom")
            Q[(state, action)] = 0
            return random_action

        random_value = random()
        if random_value < self.epsilon:
            random_action = choice(legal_actions)
            #print("purely random")
            Q[(state, random_action)] = 0
            return random_action
        else:
            #print("best")
            return self.best_action(Q, state, legal_actions)

    # Returns move_score,  new_room, new_position tuple, and boolean game over
    def move_player (self, gigel_position, Q, state, strategy):
        #print(len(Q))
        reward = MOVE_REWARD
        action = None
        actions = self.legal_next_moves(gigel_position)
        if strategy == RANDOM_STRATEGY:
            if len(actions) == 0:
                return reward, self.room_id, gigel_position, None, True
            action = choice(actions)
        elif strategy == MAX_STRATEGY:
            action = self.best_action(Q, state, actions)
        elif strategy == EPSILON_STRATEGY:
            action = self.epsilon_action(Q, state, actions)
        else:
            assert(False)

        gigel_i, gigel_j = gigel_position

        new_gigel_i = gigel_i + ACTION_EFFECTS[action][0]
        new_gigel_j = gigel_j + ACTION_EFFECTS[action][1]
        if self.matrix[new_gigel_i][new_gigel_j] == TREASURE_CHAR:
            reward = TREASURE_REWARD
            self.matrix[new_gigel_i][new_gigel_j] = " "



        new_gigel = (new_gigel_i, new_gigel_j)

        portal_entrance = self.find_portal(new_gigel)

        # Player has entered a portal !!
        if not portal_entrance == None:
            next_room, new_gigel_i, new_gigel_j = portal_entrance
            new_gigel = (new_gigel_i, new_gigel_j)
            if next_room == EXIT_CODE:
                #print("Giiigel woooon, baaaai !!")
                reward = WIN_REWARD
                return reward, PARADISE_ROOM, new_gigel, action, False
            else:
                return reward, next_room, new_gigel, action, True

        if not self.guard_position == None and new_gigel == self.guard_position:
            reward = LOOSE_REWARD
            return reward, self.room_id, new_gigel, action, False
        return reward, self.room_id, new_gigel, action, True
            

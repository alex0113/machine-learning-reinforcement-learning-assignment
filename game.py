import itertools
from room import Room
from game_utils import EXIT_CODE, PARADISE_ROOM, valid_coordinates, ACTIONS, LOOSE_REWARD, TIMEOUT_REWARD
class Game():
    rooms_number = 0
    rooms = []
    player_room = -1
    player_position = (-1, -1)
    round_count = 0
    portals_number = 0
    portals = {}
    score = 0
    game_over = False

    def __init__(self, args, Q):
        self.epsilon = args.epsilon
        self.Q = Q
        self.perception_ray = args.perception_ray
        self.perception_ray_guardian = args.perception_ray_guardian
        self.log_file = args.log_file
        self.max_rounds = args.max_rounds
        self.read_from_file(args.map_file)

    def __str__(self):
        if self.player_room == PARADISE_ROOM:
            return "$$$$$$$$$$$$$$$$"
        ret_s = "Round " + str(self.round_count) + "\n"
        ret_s = ret_s +  "Score " + str(self.score) + "\n"
        current_room = self.rooms[self.player_room]
        ret_s += str(current_room)
        ret_s += "\n\n"

        return ret_s

    def read_from_file(self, file_name):
        with open(file_name, "r") as f:
            self.rooms_number = int(f.readline().strip ())
            self.rooms = [None for k in range(self.rooms_number)]

            for room_id in range(self.rooms_number):
                height, width = map(int, f.readline().strip ().split( ))
                guard_i, guard_j = map(int, f.readline().strip ().split( ))
                room_matrix = [[] for k in range(height)]

                for i in range(height):
                     l = list(map(list, f.readline().strip()))
                     room_matrix[i] = list(itertools.chain.from_iterable(l))

                self.rooms[room_id] = Room(room_id, room_matrix, (guard_i, guard_j), self.perception_ray, self.perception_ray_guardian, self.epsilon)
            self.player_room, player_i, player_j = map(int, f.readline().strip ().split( ))
            self.player_position = (player_i, player_j)

            self.portals_number = int(f.readline().strip ())

            for portal_id in range(self.portals_number):
                p1, p2 = map(int, f.readline().strip ().split( ))

                i1, j1 = map(int, f.readline().strip ().split( ))
                i2, j2 = map(int, f.readline().strip ().split( ))
                #self.portals[(p1, p2)] = ((i1, j1), (i2, j2))
                if not p1 == EXIT_CODE:
                    self.rooms[p1].portals[(p1, p2)] = ((i1, j1), (i2, j2))
                if not p2 == EXIT_CODE:
                    self.rooms[p2].portals[(p2, p1)] = ((i2, j2), (i1, j1))

    def output_to_file(self):
        arg = ""
        if self.round_count == 0:
            arg = "w"
        else:
            arg = "a+"

        with open(self.log_file, arg) as f:
            f.write(str(self))
            f.close()

    def update_guardian_positions(self):
        for i in range(self.rooms_number):
            if i == self.player_room:
                ret_value = self.rooms[i].move_guardian(self.player_position)
                if not ret_value:
                    return False
            else:
                self.rooms[i].move_guardian()
        return True

    def update_player_position (self, state, strategy):
        move_score, self.player_room, self.player_position, action, ret_value = self.rooms[self.player_room].move_player (self.player_position,self.Q, state, strategy)

        self.score += move_score
        return ret_value, move_score, action

    def round(self, strategy):
        old_state = self.compute_state()
        return_value = self.update_guardian_positions()
        if not return_value:
            print("A guardian caught Gigel")
            self.game_over = True

        return_value, move_score, action = self.update_player_position(old_state, strategy)
        if not return_value:
            if move_score == LOOSE_REWARD:
                print("Gigel foolishly went to a guardian")
            else:
                print("Gigel wooon")
            self.game_over = True


        new_state = self.compute_state()
        self.round_count += 1

        if not self.player_room == PARADISE_ROOM:
            current_room = self.rooms[self.player_room]
            current_room.player_position = self.player_position

        if self.round_count == self.max_rounds -1:
            move_score = TIMEOUT_REWARD
            print("TIMEOUT_REWARD")
        return move_score, action, old_state, new_state

    def is_over(self):
        if self.game_over:
            return True

        if self.round_count > self.max_rounds:
            print("Timeout",  self.max_rounds)
            return True
        return False

    def compute_state (self):
        state = str(self.player_room) + "\n"
        if self.player_room == PARADISE_ROOM:
            return state
        state += self.rooms[self.player_room].compute_state ()
        #print(state)
        return state

    def get_legal_actions(self):
        if self.player_room == PARADISE_ROOM:
            return ACTIONS
        return self.rooms[self.player_room].legal_next_moves (self.player_position)



        

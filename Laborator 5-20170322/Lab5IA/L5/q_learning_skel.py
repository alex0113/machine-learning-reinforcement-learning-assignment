# Tudor Berariu, 2016

# General imports
from copy import copy
from random import choice, random
from argparse import ArgumentParser
from time import sleep

# Game functions
from mini_pacman import ( get_initial_state,       # get initial state from file
                          get_legal_actions,  # get the legal actions in a state
                          is_final_state,         # check if a state is terminal
                          apply_action,       # apply an action in a given state
                          display_state )            # display the current state

def epsilon_greedy(Q, state, legal_actions, epsilon):
    # TODO (2) : Epsilon greedy
    unexp_actions = []
    for a in legal_actions:
        if (state, a) not in Q.keys():
            unexp_actions.append(a)

    if len(unexp_actions) != 0:
        return choice(unexp_actions)

    if random() > epsilon:
        return best_action(Q, state, legal_actions)

    return choice(legal_actions)

def best_action(Q, state, legal_actions):
    # TODO (3) : Best action
    maxr = -99999
    maxa = choice(legal_actions)
    for a in legal_actions:
        val = Q.get((state, a), None)
        if val != None and val > maxr:
            maxr = val
            maxa = a

    return maxa


def q_learning(args):
    Q = {}
    train_scores = []
    eval_scores = []
    episode = 0
                                                          # for each episode ...
    for train_ep in range(1, args.train_episodes + 1):

                                                    # ... get the initial state,
        score = 0
        state = get_initial_state(args.map_file)
        episode += 1

                                               # display current state and sleep
        if args.verbose:
            display_state(state); sleep(args.sleep)

                                           # while current state is not terminal
        while not is_final_state(state, score):

                                               # choose one of the legal actions
            actions = get_legal_actions(state)
            action = epsilon_greedy(Q, state, actions, 0.05)

                            # apply action and get the next state and the reward
            next_state, reward, msg = apply_action(state, action)
            score += reward

            # TODO (1) : Q-Learning
            next_actions = get_legal_actions(next_state)
            # print(next_states)

            max = -99999
            for a in next_actions:
               value = Q.get((next_state, a), 0)
               if value > max :
                   max = value

            aux = reward + args.discount * max - Q.get((state, action), 0)
            Q[(state, action)] = Q.get((state, action), 0) + args.learning_rate * aux

            state = next_state


                                               # display current state and sleep
            if args.verbose:
                print(msg); display_state(state); sleep(args.sleep)

        print("Episode %6d / %6d" % (train_ep, args.train_episodes))
        train_scores.append(score)

                                                    # evaluate the greedy policy
        if train_ep % args.eval_every == 0:
            avg_score = .0

            # TODO (4) : Evaluate
            for ep in range(0, 2):
                state = get_initial_state(args.map_file)
                score = 0
                while not is_final_state(state, score):
                    actions = get_legal_actions(state)
                    bestAction = epsilon_greedy(Q, state, actions, 0.05)

                    state, reward, msg = apply_action(state, bestAction)
                    #print(reward)
                    score += reward

                avg_score += score

            avg_score /= 2
            eval_scores.append(avg_score)

        #args.epsilon -= 0.0005

    # --------------------------------------------------------------------------
    if args.final_show:
        state = get_initial_state(args.map_file)
        final_score = 0
        while not is_final_state(state, final_score):
            action = best_action(Q, state, get_legal_actions(state))
            state, reward, msg = apply_action(state, action)
            final_score += reward
            print(msg); display_state(state); sleep(args.sleep)

    if args.plot_scores:
        from matplotlib import pyplot as plt
        import numpy as np
        plt.xlabel("Episode")
        plt.ylabel("Average score")
        plt.plot(
            np.linspace(1, args.train_episodes, args.train_episodes),
            np.convolve(train_scores, [0.2,0.2,0.2,0.2,0.2], "same"),
            linewidth = 1.0, color = "blue"
        )
        plt.plot(
            np.linspace(args.eval_every, args.train_episodes, len(eval_scores)),
            eval_scores, linewidth = 2.0, color = "red"
        )
        plt.show()

if __name__ == "__main__":
    parser = ArgumentParser()
    # Input file
    parser.add_argument("--map_file", type = str, default = "mini_map",
                        help = "File to read map from.")
    # Meta-parameters
    parser.add_argument("--learning_rate", type = float, default = 0.2,
                        help = "Learning rate")
    parser.add_argument("--discount", type = float, default = 0.99,
                        help = "Value for the discount factor")
    parser.add_argument("--epsilon", type = float, default = 0.05,
                        help = "Probability to choose a random action.")
    # Training and evaluation episodes
    parser.add_argument("--train_episodes", type = int, default = 1000,
                        help = "Number of episodes")
    parser.add_argument("--eval_every", type = int, default = 10,
                        help = "Evaluate policy every ... games.")
    parser.add_argument("--eval_episodes", type = float, default = 10,
                        help = "Number of games to play for evaluation.")
    # Display
    parser.add_argument("--verbose", dest="verbose",
                        action = "store_true", help = "Print each state")
    parser.add_argument("--plot", dest="plot_scores", action="store_true",
                        help = "Plot scores in the end")
    parser.add_argument("--sleep", type = float, default = 0.1,
                        help = "Seconds to 'sleep' between moves.")
    parser.add_argument("--final_show", dest = "final_show",
                        action = "store_true",
                        help = "Demonstrate final strategy.")
    args = parser.parse_args()
    q_learning(args)

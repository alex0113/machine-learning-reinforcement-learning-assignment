# Tudor Berariu, 2016

# General imports
from copy import copy
from random import choice, random
from argparse import ArgumentParser
from time import sleep

# Game functions
from mini_pacman import ( get_initial_state,       # get initial state from file
                          get_legal_actions,  # get the legal actions in a state
                          is_final_state,         # check if a state is terminal
                          apply_action,       # apply an action in a given state
                          display_state )            # display the current state

def epsilon_greedy(Q, state, legal_actions, epsilon):
    explored_actions = Q.keys()

    unexplored_actions = []
    for action in legal_actions:
        if not((state, action) in explored_actions):
            #print("Adaugam ", action)
            unexplored_actions.append(action)

    if unexplored_actions:
        #print("UNEXPLORED", unexplored_actions)
        return choice(unexplored_actions)

    value = random()
    if value < epsilon:
        #print("RANDOM EPSILON")
        return choice(legal_actions)
    #print("BEST ACTION")
    return best_action(Q, state, legal_actions)

def best_action(Q, state, legal_actions):
    maxReward = 0
    maxAction = None
    for action in legal_actions:
        new_state, reward, msg = apply_action(state, action)
        if reward > maxReward:
            maxReward = reward
            maxAction = action

    if maxReward == 0:
        #print(">>> Nu am gasit, asa ca dam cu random")
        return choice(legal_actions)

    #print("Am gasit o solutie buna", maxAction)
    return maxAction

def q_learning(args):
    print(args)

    Q = {}
    train_scores = []
    eval_scores = []
                                                          # for each episode ...
    for train_ep in range(1, args.train_episodes + 1):

                                                    # ... get the initial state,
        score = 0
        state = get_initial_state(args.map_file)

                                               # display current state and sleep
        if args.verbose:
            display_state(state); sleep(args.sleep)

                                           # while current state is not terminal
        while not is_final_state(state, score):

                                               # choose one of the legal actions
            actions = get_legal_actions(state)
            action = epsilon_greedy(Q, state, actions, args.epsilon)

                            # apply action and get the next state and the reward
            new_state, reward, msg = apply_action(state, action)
            score += reward

            # TODO (1) : Q-Learning
            if not((state, action) in Q.keys()):
                Q[(state,action)] = 0

            new_actions = get_legal_actions(new_state)

            maxTranzition = 0
            for new_action in new_actions:
                if not((new_state, new_action) in Q.keys()):
                    Q[(new_state, new_action)] = 0
                if Q[(new_state, new_action)] > maxTranzition:
                    maxTranzition = Q[(new_state, new_action)]

            Q[(state, action)] = Q[(state, action)] + args.learning_rate * (reward + args.discount * maxTranzition - Q[(state, action)])

            state = new_state
                                               # display current state and sleep
            if args.verbose:
                print(msg); display_state(state); sleep(args.sleep)

        #print("Episode %6d / %6d" % (train_ep, args.train_episodes))
        train_scores.append(score)

                                                    # evaluate the greedy policy
        if train_ep % args.eval_every == 0:
            avg_score = .0

            sumScores = .0
            for score in train_scores:
                sumScores += score
            avg_score = sumScores / len(train_scores)
            # TODO (4) : Evaluate

            eval_scores.append(avg_score)
            print(avg_score)

    # --------------------------------------------------------------------------
    if args.final_show:
        state = get_initial_state(args.map_file)
        final_score = 0
        while not is_final_state(state, final_score):
            action = best_action(Q, state, get_legal_actions(state))
            state, reward, msg = apply_action(state, action)
            final_score += reward
            print(msg); display_state(state); sleep(args.sleep)

    if args.plot_scores:
        from matplotlib import pyplot as plt
        import numpy as np
        plt.xlabel("Episode")
        plt.ylabel("Average score")
        plt.plot(
            np.linspace(1, args.train_episodes, args.train_episodes),
            np.convolve(train_scores, [0.2,0.2,0.2,0.2,0.2], "same"),
            linewidth = 1.0, color = "blue"
        )
        plt.plot(
            np.linspace(args.eval_every, args.train_episodes, len(eval_scores)),
            eval_scores, linewidth = 2.0, color = "red"
        )
        plt.show()

if __name__ == "__main__":
    parser = ArgumentParser()
    # Input file
    parser.add_argument("--map_file", type = str, default = "mini_map",
                        help = "File to read map from.")
    # Meta-parameters
    parser.add_argument("--learning_rate", type = float, default = 0.1,
                        help = "Learning rate")
    parser.add_argument("--discount", type = float, default = 0.99,
                        help = "Value for the discount factor")
    parser.add_argument("--epsilon", type = float, default = 0.05,
                        help = "Probability to choose a random action.")
    # Training and evaluation episodes
    parser.add_argument("--train_episodes", type = int, default = 1000,
                        help = "Number of episodes")
    parser.add_argument("--eval_every", type = int, default = 10,
                        help = "Evaluate policy every ... games.")
    parser.add_argument("--eval_episodes", type = float, default = 10,
                        help = "Number of games to play for evaluation.")
    # Display
    parser.add_argument("--verbose", dest="verbose",
                        action = "store_true", help = "Print each state")
    parser.add_argument("--plot", dest="plot_scores", action="store_true",
                        help = "Plot scores in the end")
    parser.add_argument("--sleep", type = float, default = 0.1,
                        help = "Seconds to 'sleep' between moves.")
    parser.add_argument("--final_show", dest = "final_show",
                        action = "store_true",
                        help = "Demonstrate final strategy.")
    args = parser.parse_args()
    q_learning(args)
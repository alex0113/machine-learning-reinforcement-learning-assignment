# Tudor Berariu, 2016

# General imports
from copy import copy
from random import choice, random
from argparse import ArgumentParser
from time import sleep
alpha = 0.5
theta = 0.1

# Game functions
from mini_pacman import ( get_initial_state,       # get initial state from file
                          get_legal_actions,  # get the legal actions in a state
                          is_final_state,         # check if a state is terminal
                          apply_action,       # apply an action in a given state
                          display_state )            # display the current state

def epsilon_greedy(Q, state, legal_actions, epsilon):
    # TODO (2) : Epsilon greedy


    not_explored_actions = []
    for action in legal_actions:
        if not (state, action) in Q:
            not_explored_actions.append(action)
    best_action_found = best_action(Q, state, legal_actions)
    random_value = random()
    if len(not_explored_actions) > 0 :
        random_action = choice(not_explored_actions)
        Q[(state, random_action)] = 0
        return random_action
    if random_value <epsilon:
        return choice(legal_actions)
    else:
        return best_action_found

def best_action(Q, state, legal_actions):
    # TODO (3) : Best action
    max_val = -9999
    max_action = choice(legal_actions)
    for action in legal_actions:
        if (state, action) in Q:
            reward = Q[(state, action)]
        else:
            continue

        if reward > max_val:
            max_val = reward
            max_action = action

    if not (state, max_action) in Q:
        Q[(state, max_action)] = 0

    return max_action

def q_learning(args):
    Q = {}
    train_scores = []
    eval_scores = []
                                                          # for each episode ...
    for train_ep in range(1, args.train_episodes + 1):

                                                    # ... get the initial state,
        score = 0
        state = get_initial_state(args.map_file)

                                               # display current state and sleep
        if args.verbose:
            display_state(state); sleep(args.sleep)

                                           # while current state is not terminal
        while not is_final_state(state, score):

                                               # choose one of the legal actions
            actions = get_legal_actions(state)
            #print(state)
            action = epsilon_greedy(Q, state, actions, args.epsilon)

                            # apply action and get the next state and the reward
            #print (action)
            next_state, reward, msg = apply_action(state, action)
            score += reward
            #print(score)

            # TODO (1) : Q-Learning
            max_q_prim = -9999
            next_actions = get_legal_actions(next_state)
            for next_action in next_actions:
                if not (next_state, next_action) in Q:
                    r = 0
                else:
                    r = Q[(next_state, next_action)]
                if r > max_q_prim:
                    max_q_prim = r

            old_value = 0
            if (state, action) in Q:
                old_value = Q[(state, action)]

            Q[(state,action)] = old_value + args.learning_rate * (reward + args.discount*max_q_prim-old_value)
                                               # display current state and sleep
            state = next_state

            if args.verbose:
                print(msg); display_state(state); sleep(args.sleep)

        print("Episode %6d / %6d" % (train_ep, args.train_episodes))
        train_scores.append(score)

                                                    # evaluate the greedy policy
        if train_ep % args.eval_every == 0:
            avg_score = .0

            # TODO (4) : Evaluate
            print("here")
            for i in range(int(args.eval_episodes)):
                state2 = get_initial_state(args.map_file)
                game_score = 0
                count = 0
                while not is_final_state(state2, game_score):
                    count+= 1
                    legal_actions = get_legal_actions(state2)
                    action = best_action(Q, state2, legal_actions)
                    next_state, reward, msg = apply_action(state2, action)
                    game_score += reward
                    state2 = next_state
                    if count > 1000:
                        print(msg, reward); display_state(state); sleep(args.sleep)

                        game_score = 0
                    if count > 1004:
                        print("Break")
                        break
                avg_score += game_score
                #print(score)
            avg_score /= args.eval_episodes
            eval_scores.append(avg_score)

    # --------------------------------------------------------------------------
    if args.final_show:
        state = get_initial_state(args.map_file)
        final_score = 0
        while not is_final_state(state, final_score):
            action = best_action(Q, state, get_legal_actions(state))
            state, reward, msg = apply_action(state, action)
            final_score += reward
            print(msg); display_state(state); sleep(args.sleep)

    if args.plot_scores:
        print("Plot")
        from matplotlib import pyplot as plt
        import numpy as np
        plt.xlabel("Episode")
        plt.ylabel("Average score")
        plt.plot(
            np.linspace(1, args.train_episodes, args.train_episodes),
            np.convolve(train_scores, [0.2,0.2,0.2,0.2,0.2], "same"),
            linewidth = 1.0, color = "blue"
        )
        plt.plot(
            np.linspace(args.eval_every, args.train_episodes, len(eval_scores)),
            eval_scores, linewidth = 2.0, color = "red"
        )
        plt.show()

if __name__ == "__main__":
    parser = ArgumentParser()
    # Input file
    parser.add_argument("--map_file", type = str, default = "mini_map",
                        help = "File to read map from.")
    # Meta-parameters
    parser.add_argument("--learning_rate", type = float, default = 0.1,
                        help = "Learning rate")
    parser.add_argument("--discount", type = float, default = 0.99,
                        help = "Value for the discount factor")
    parser.add_argument("--epsilon", type = float, default = 0.05,
                        help = "Probability to choose a random action.")
    # Training and evaluation episodes
    parser.add_argument("--train_episodes", type = int, default = 1000,
                        help = "Number of episodes")
    parser.add_argument("--eval_every", type = int, default = 10,
                        help = "Evaluate policy every ... games.")
    parser.add_argument("--eval_episodes", type = float, default = 10,
                        help = "Number of games to play for evaluation.")
    # Display
    parser.add_argument("--verbose", dest="verbose",
                        action = "store_true", help = "Print each state")
    parser.add_argument("--plot", dest="plot_scores", action="store_true",
                        help = "Plot scores in the end")
    parser.add_argument("--sleep", type = float, default = 0.1,
                        help = "Seconds to 'sleep' between moves.")
    parser.add_argument("--final_show", dest = "final_show",
                        action = "store_true",
                        help = "Demonstrate final strategy.")
    args = parser.parse_args()
    q_learning(args)

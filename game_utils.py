from math import fabs
ACTIONS = ["UP", "RIGHT", "DOWN", "LEFT", "STAY"]
ACTION_EFFECTS = {
    "UP": (-1,0),
    "RIGHT": (0,1),
    "DOWN": (1,0),
    "LEFT": (0,-1),
    "STAY": (0,0)
}
RANDOM_STRATEGY = "RANDOM"
MAX_STRATEGY = "MAX"
EPSILON_STRATEGY = "EPSILON"

GUARDIAN_CHAR = "G"
PLAYER_CHAR = "P"
PLAYER_INCOGNITO_CHAR = "p"
PORTAL_CHAR = "O"
TREASURE_CHAR = "$"
PORTAL_WALL = "*"

TREASURE_REWARD = 2
LOOSE_REWARD = -50
WIN_REWARD = 200
MOVE_REWARD = -0.1
TIMEOUT_REWARD = -100

# Random big number
EXIT_CODE = 3549
PARADISE_ROOM = 3425

def valid_coordinates(i, j, height, width):
    if i<0 or j<0 or i>height-1 or j > width - 1:
        return False
    return True

def distance_between(a, b):
    ai, aj = a
    bi, bj = b
    return (ai - bi) * (ai - bi) + (aj - bj) * (aj - bj)

def distance_lt(a, b, perception_ray):
    ai, aj = a
    bi, bj = b
    if fabs(ai - bi) < perception_ray and fabs(aj - bj)< perception_ray:
        return True
    return False

